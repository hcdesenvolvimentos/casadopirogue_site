<?php



/**

 * Plugin Name: Base Casa do Pirogue

 * Description: Controle base do tema Casa do Pirogue.

 * Version: 0.1

 * Author: Agência Carolinos Desenvolvimento

 * Author URI:

 * Licence: GPL2

 */





function baseCasadopirogue () {



		// TIPOS DE CONTEÚDO

		conteudosCasadopirogue();



		// TAXONOMIA

		taxonomiaCasadopirogue();



		// META BOXES

		metaboxesCasadopirogue();



		// SHORTCODES

		// shortcodesCasadopirogue();



	    // ATALHOS VISUAL COMPOSER

	    //visualcomposerCasadopirogue();



	}



	/****************************************************

	* TIPOS DE CONTEÚDO

	*****************************************************/



	function conteudosCasadopirogue (){



		// TIPOS DE CONTEÚDO

		

		tipoDestaques();



		tipoProdutos();

		

	



		/* ALTERAÇÃO DO TÍTULO PADRÃO */

		add_filter( 'enter_title_here', 'trocarTituloPadrao' );

		function trocarTituloPadrao($titulo){



			switch (get_current_screen()->post_type) {



				case 'produto':

					$titulo = 'nome do produto';

				break;



				case 'destaques':

					$titulo = 'Nome do destaque';

				break;			



				default:

				break;

			}



		    return $titulo;



		}



	}







		// CUSTOM POST TYPE DESTAQUES

		function tipoDestaques() {



			$rotulosDestaques = array(

									'name'               => 'Destaques',

									'singular_name'      => 'destaque',

									'menu_name'          => 'Destaques',

									'name_admin_bar'     => 'destaques',

									'add_new'            => 'Adicionar novo',

									'add_new_item'       => 'Adicionar novo destaque',

									'new_item'           => 'Novo destaque',

									'edit_item'          => 'Editar destaque',

									'view_item'          => 'Ver destaque',

									'all_items'          => 'Todos os destaques',

									'search_items'       => 'Buscar destaques',

									'parent_item_colon'  => 'Dos destaques',

									'not_found'          => 'Nenhum destaque cadastrado.',

									'not_found_in_trash' => 'Nenhum destaque na lixeira.'

								);



			$argsDestaques 	= array(

									'labels'             => $rotulosDestaques,

									'public'             => true,

									'publicly_queryable' => true,

									'show_ui'            => true,

									'show_in_menu'       => true,

									'menu_position'		 => 4,

									'menu_icon'          => 'dashicons-images-alt2',

									'query_var'          => true,

									'rewrite'            => array( 'slug' => 'destaques' ),

									'capability_type'    => 'post',

									'has_archive'        => true,

									'hierarchical'       => false,

									'supports'           => array( 'title', 'thumbnail', 'editor' )

								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('destaques', $argsDestaques);



		}



		// CUSTOM POST TYPE PRODUTO

		function tipoProdutos() {



			$rotulosProdutos = array(

									'name'               => 'Produtos',

									'singular_name'      => 'produto',

									'menu_name'          => 'Produtos',

									'name_admin_bar'     => 'produtos',

									'add_new'            => 'Adicionar novo',

									'add_new_item'       => 'Adicionar novo produto',

									'new_item'           => 'Novo produto',

									'edit_item'          => 'Editar produto',

									'view_item'          => 'Ver produto',

									'all_items'          => 'Todos os produtos',

									'search_items'       => 'Buscar produto',

									'parent_item_colon'  => 'Dos produtos',

									'not_found'          => 'Nenhum produto cadastrado.',

									'not_found_in_trash' => 'Nenhum produto na lixeira.'

								);



			$argsProdutos 	= array(

									'labels'             => $rotulosProdutos,

									'public'             => true,

									'publicly_queryable' => true,

									'show_ui'            => true,

									'show_in_menu'       => true,

									'menu_position'		 => 4,

									'menu_icon'          => 'dashicons-store',

									'query_var'          => true,

									'rewrite'            => array( 'slug' => 'produtos' ),

									'capability_type'    => 'post',

									'has_archive'        => true,

									'hierarchical'       => false,

									'supports'           => array( 'title', 'thumbnail', 'editor' )

								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('produtos', $argsProdutos);



		}

	





	/****************************************************

	* TAXONOMIA

	*****************************************************/

	function taxonomiaCasadopirogue () {



		taxonomiaCategoriaProdutos();





	}

		// TAXONOMIA DE CASES

		function taxonomiaCategoriaProdutos() {



			$rotulosCategoriaProdutos = array(

												'name'              => 'Categorias de produto',

												'singular_name'     => 'Categoria de produto',

												'search_items'      => 'Buscar categoria de produto',

												'all_items'         => 'Todas categorias de produto',

												'parent_item'       => 'Categoria de produto pai',

												'parent_item_colon' => 'Categoria de produto pai:',

												'edit_item'         => 'Editar categoria de produto',

												'update_item'       => 'Atualizar categoria de produtos',

												'add_new_item'      => 'Nova categoria de produto',

												'new_item_name'     => 'Nova categoria',

												'menu_name'         => 'Categorias de produto',

											);



			$argsCategoriaProdutos 		= array(

												'hierarchical'      => true,

												'labels'            => $rotulosCategoriaProdutos,

												'show_ui'           => true,

												'show_admin_column' => true,

												'query_var'         => true,

												'rewrite'           => array( 'slug' => 'categoria-produtos' ),

											);



			register_taxonomy( 'categoriaProdutos', array( 'produtos' ), $argsCategoriaProdutos );



		}







		

    /****************************************************

	* META BOXES

	*****************************************************/

	function metaboxesCasadopirogue(){



		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );



	}



		function registraMetaboxes( $metaboxes ){



			$prefix = 'Casadopirogue_';



	

			// METABOX DE PRODUTOS

			$metaboxes[] = array(



				'id'			=> 'detalhesMetaboxProdutos',

				'title'			=> 'Detalhes do produto / Informações nutricional',

				'pages' 		=> array( 'produtos' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(			



					array(

                        'name'  => 'Preço do pacote:',

                        'id'    => "{$prefix}preco",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'  => 'R$ 17,00 ',

                        

                    ),
                    array(

                        'name'  => 'Unidade:',

                        'id'    => "{$prefix}unidade",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'  => 'Contém 15 unidades (700g). ',

                        

                    ),

					array(

                        'name'  => 'Ingredientes:',

                        'id'    => "{$prefix}ingredientes",

                        'desc'  => '',

                        'type'  => 'textarea',

                        

                    ),

                     array(

                        'name'  => 'Porção de:',

                        'id'    => "{$prefix}porcao",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'100 g (2 un.)'

                        

                    ),

                       array(

                        'name'  => 'Glútem e conservantes:',

                        'id'    => "{$prefix}glutem-conservantes",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'contem glutem /não contem conservantes'

                        

                    ),

                    array(

                        'name'  => 'Valor energético:',

                        'id'    => "{$prefix}valor-energetico",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    ),

                    array(

                        'name'  => 'Carboidratos:',

                        'id'    => "{$prefix}carboidratos",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    ),

                    array(

                        'name'  => 'Proteinas:',

                        'id'    => "{$prefix}proteinas",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    ),

                    array(

                        'name'  => 'Gorduras totais:',

                        'id'    => "{$prefix}gorduras-totais",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    ),

                    array(

                        'name'  => 'Gorduras saturadas:',

                        'id'    => "{$prefix}gorduras-saturadas",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    ),

                    array(

                        'name'  => 'Gorduras trans:',

                        'id'    => "{$prefix}gorduras-trans",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    ),

                    array(

                        'name'  => 'Fibra alimentar:',

                        'id'    => "{$prefix}fibra-alimentar",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    ),

                    array(

                        'name'  => 'Sódio:',

                        'id'    => "{$prefix}sodio",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    ),

                    array(

                        'name'  => 'Cálcio:',

                        'id'    => "{$prefix}calcio",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'4,5g;20%'

                        

                    ),

                    array(

                        'name'  => 'Colesterol:',

                        'id'    => "{$prefix}colesterol",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'4,5g;20%'

                        

                    ),

                    array(

                        'name'  => 'Ferro:',

                        'id'    => "{$prefix}ferro",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'24g;8%'

                        

                    )

                  

				)

			

			);



			// METABOX DE DESTAQUE

			$metaboxes[] = array(



				'id'			=> 'detalhesMetaboxDestaque',

				'title'			=> 'Detalhes do destaque',

				'pages' 		=> array( 'destaques' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(			

 

                    array(

                        'name'  => 'Link do destaque:',

                        'id'    => "{$prefix}link-destaque",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'Link do destaque'

                        

                    ),

                     array(

                        'name'  => 'Cor da lente:',

                        'id'    => "{$prefix}cor-destaque",

                        'desc'  => '',

                        'type'  => 'text',

                        'placeholder'=>'Cor em RGBA'

                        

                    ),          

                  

				)

			

			);

			



	



			return $metaboxes;

		}



	/****************************************************

	* SHORTCODES

	*****************************************************/

	function shortcodesCasadopirogue(){



	}



	/****************************************************

	* ATALHOS VISUAL COMPOSER

	*****************************************************/

	function visualcomposerCasadopirogue(){



	    if (class_exists('WPBakeryVisualComposer')){



		}



	}



  	/****************************************************

	* AÇÕES

	*****************************************************/



	// INICIA A FUNÇÃO PRINCIPAL

	add_action('init', 'baseCasadopirogue');



	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES

	//add_action( 'add_meta_boxes', 'metaboxjs');



	// FLUSHS

	function rewrite_flush() {



    	baseCasadopirogue();



   		flush_rewrite_rules();

	}



	register_activation_hook( __FILE__, 'rewrite_flush' );