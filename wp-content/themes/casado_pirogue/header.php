<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Casa_do_Pirogue
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url'); ?>/img/favicon.ico" />
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79852475-1', 'auto');
  ga('send', 'pageview');

</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-8558851584430625",
    enable_page_level_ads: true
  });
</script>
</head>

<body <?php body_class(); ?>>

<!-- TOPO -->
		<div class="topo" id="topo">
			<nav class="navbar" role="navigation">

				<div class="container">
					
					<div class="row">
						<div class="col-md-3">	
							<!-- LOGO -->
							<a href="<?php echo home_url('/'); ?>" title="Casa do Pirogue">
								<h1>
									Casa do Pirogue
								</h1>
							</a>
							<!-- LOGOTIPO / MENU MOBILE-->
							<div class="navbar-header">
								

								<!-- MENU MOBILE TRIGGER -->
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
									<span class="sr-only"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>

							</div>
						</div>
						<div class="col-md-9 text-center">
							<!-- MENU -->
							<nav class="collapse navbar-collapse" id="collapse">

								<ul class="nav navbar-nav">

									<li><a href="<?php echo home_url('/'); ?>" id="home">Home</a></li>
									<li><a href="<?php if (is_front_page()) { echo "#area-produtos";} else { echo home_url('/produtos/');} ?>" id="produtos">Nossos Produtos</a></li>
									<li><a href="https://delivery.casadopirogue.com.br/" id="pedidos-oline">Pedidos Online</a></li>
									<li><a href="<?php echo home_url('/sobre-nos/'); ?>" id="sobre-nos">Sobre nós</a></li>									
									<li><a href="<?php if (is_front_page()) { echo "#area-contato";} else { echo site_url('/#area-contato');} ?>" id="contato">Contato</a></li>

								</ul>

							</nav>
						</div>
					</div>
				</div>
			</nav>
		</div>

		