<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Casa_do_Pirogue
 */

get_header(); ?>

	<div class="pg pg-error">
			<div class="container">
				<p>"Ops não encontrou oque precisava ?"</p>

				<div class="link">
					<a href="<?php if (is_front_page()) { echo "#area-contato";} else { echo site_url('/#area-contato');} ?>">Entre em contato conosco !</a>
					<a href="<?php echo home_url('/inicial/'); ?>">voltar para Inicial</a>
					<a href="<?php echo home_url('/delivery/dashboard/'); ?>">Minha conta</a>

				</div>
			</div>	
		</div>
<?php
get_footer();
