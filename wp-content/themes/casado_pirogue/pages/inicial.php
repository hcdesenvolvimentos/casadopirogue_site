

<?php
/**
 * Template Name: Inicial
 * Description: 
 *
 * @package Casa_do_Pirogue
 */
global $configuracao;
global $post;

 $delivey_telefone = $configuracao['opt-telefone-delivery'];
 $endereco = $configuracao['opt-endereco-rua'];
 $cidade = $configuracao['opt-endereco-cidade'];
 $foto_quem_somos = $configuracao['opt-foto'];
 $texto_quem_somos = $configuracao['opt-texto'];
 $foto_ilustrativa = $configuracao['opt-foto-fundo'];

get_header(); ?>
	
	<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		
		<!-- CARROSSEL -->
		<div class="carrossel">
			<!-- BOTÕES DE NAVEGAÇÃO -->
			
			<button id="botao" class="navegacaoDestqueFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
			<button class="navegacaoDestqueTras hidden-xs"><i class="fa fa-angle-right"></i></button>
			
			<div id="carrossel-inicial" class="owlCarousel" >

				
            <?php 

                // LOOP DE DESTAQUE

                $destaquesPost = new WP_Query( array( 'post_type' => 'destaques', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
               
                while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();
                
                $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

                $foto = $foto[0];

                $tipo = rwmb_meta('Casadopirogue_link-destaque');

            ?>

				<div class="item">
					<a href="<?php echo $tipo ?>" target="_blank">
						<div class="foto-destaque" style="background:url(<?php echo $foto ?>);">
							<div class="lente" style="background: <?php echo rwmb_meta('Casadopirogue_cor-destaque'); ?>; ?>"><h4><?php echo get_the_content() ?></h4></div>
						</div>
					</a>

				</div>
		  	<?php endwhile; wp_reset_query(); ?>    
			</div>
			
		
			<div class="info-contato hvr-wobble-vertical" id="telefone">
				<div class="delivery" id="delivery">Delivery</div>
				<span><?php echo  $delivey_telefone ?></span>
				<p id="area-produtos"><?php echo $endereco  ?> <?php echo $cidade?></p>
			</div>
			
			<div class="botoes-pedido" >
				<a href="https://delivery.casadopirogue.com.br/">
					<div class="correcaoY">
						<div class="botao-online"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i></div>
						<span>Peça Online</span>
					</div>
				</a>
				<div class="correcaoY" id="Iconeligar">
					<div class="botao-ligar"><i class="fa fa-phone" aria-hidden="true"></i></div>
					<span>Ligue agora</span>
				</div>
			</div>

		</div>
		
		
		<!-- PRODUTOS -->
		<div class="container">
			<div class="titulo">
				Nossos Produtos
			</div>

			<!-- LISTA DE PRODUTOS -->
			<div class="produtos">
				<ul>
					
                    <?php
                        // DEFINE A TAXONOMIA

                        $taxonomia = 'categoriaProdutos';

                        // LISTA AS CATEGORIAS PAI DOS SABORES

                        $categoriaProdutos = get_terms( $taxonomia, array(

                            'orderby'    => 'count',

                            'hide_empty' => 0,

                            'parent'     => 0

                        ));               
                                              

                        foreach ($categoriaProdutos as $categoriaProduto) {


                           
                            $nome = $categoriaProduto->name;                           


                            $categoriaAtivaImg = z_taxonomy_image_url($categoriaProduto->term_id);

                            if ($nome != "Pastel Integral") {
                            
                            

                           	
                    ?>

					<li>
						<a href="<?php echo get_category_link($categoriaProduto->term_id); ?>">
							<div class="foto-produto" style="background: url(<?php echo $categoriaAtivaImg ?>);">
								<div class="lente">
									<h2>
									<?php  
										if ($nome == "Pirogues recheados") {

											echo "Pirogue";
										}else{
											echo "Pastel";
										}

									?>
									</h2>
								</div>
							</div>
						</a>
					</li>				
					
                    <?php } } ?>  
				
				</ul>

				<div class="todos-produtos">
					<a href="<?php echo home_url('nossos-produtos/'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/lista.png" alt="">Ver todos os produtos</a>
				</div>
			</div>
			
		</div>

		<!-- SOBRE NÓS -->
		<div class="bg-fundo" style="background: url(<?php echo $foto_ilustrativa['url'] ?>);">
			<div class="lente">

				<div class="container">
					<div class="titulo">
						Sobre nós
					</div>
					<div class="sobre-nos">
						<div class="foto" style="background: url(<?php echo $foto_quem_somos['url']; ?>);"></div>
						<p><?php echo $texto_quem_somos ?></p>
					</div>
				</div>

			</div>
			<div id="area-contato"></div>
		</div>

		<!-- CONTATO -->
		<div class="container">
			<div class="titulo">
				Contato
			</div>
			<div class="contato" >
				<div class="row">
					<div class="col-md-6">
						<div class="form" >
	
						<?php

                           echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato"]');

                        ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="mapa">
							<iframe src="https://maps.google.com.br/maps?q=<?php echo $cidade; echo $endereco ; ?>&output=embed"  frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
						</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>