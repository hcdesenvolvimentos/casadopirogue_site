



<?php

/**

 * Template Name: Fretes e Entregas

 * Description: 

 *

 * @package Casa_do_Pirogue

 */

global $configuracao;







$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );	

$foto = $foto[0];

	

get_header(); ?>

	

<div class="pg pg-quem-smos">

	<div class="container">

		<div class="bg-quem-somos" style="background: url(<?php echo $foto ?>);">

			<div class="lente">

				<p><span><?php echo get_the_title();  ?></span></p>

			</div>

		</div>



		<p class="texto"><?php 	echo the_content(); ?></p>

		

		</div>

	</div>

</div>







<?php get_footer(); ?>