

<?php
/**
 * Template Name: Sobre nós
 * Description: 
 *
 * @package Casa_do_Pirogue
 */
global $configuracao;
$galeria = $configuracao['opt-gallery'];

global $post;
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );	
$foto = $foto[0];
	
get_header(); ?>
	
<div class="pg pg-quem-smos">
	<div class="container">
		<div class="bg-quem-somos" style="background: url(<?php echo $foto ?>);">
			<div class="lente">
				<p><span><?php echo get_the_title();  ?></span></p>
			</div>
		</div>

		<p class="texto"><?php 	echo get_the_content(); ?></p>

		<div class="carrossel-quem-somos">	
			<div  id="carrossel-quem-somos" class="owlCarousel">
				<?php					    
				    $myGalleryIDs = explode(',', $galeria);
				    foreach($myGalleryIDs as $myPhotoID):
				        
			    ?>
				<div class="item">	
				<a href="<?php echo wp_get_attachment_url( $myPhotoID ); ?>" id="fancy" rel="gallery1">
					<div class="bg" style="background:url(<?php echo wp_get_attachment_url( $myPhotoID ); ?>);"></div>
				</a>
				</div>
				<?php endforeach; ?>		
			</div>
		</div>
	</div>
</div>



<?php get_footer(); ?>