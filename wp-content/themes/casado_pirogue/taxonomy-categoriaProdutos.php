<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Casa_do_Pirogue
 */
global $configuracao;
$frabricacao = $configuracao['opt-fabricacao'];
$validade = $configuracao['opt-validade'];
$conservacao = $configuracao['opt-conservacao'];
$info_pacote = $configuracao['opt-info-pacote'];
//RECUPERANDO NOME DA CATERIA 
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
//RECUPERANDO A FOTO DESTACADA 
$categoriaAtivaImg = z_taxonomy_image_url($categoriaMassagem->term_id);

get_header(); ?>
	
<!-- NOSSOS PRODUTOS -->
<div class="pg pg-nossos-produtos">		
	
	<div class="bg-fundo" style="background: url(<?php echo $categoriaAtivaImg ?>);">
		<div class="lente">	
			<div class="container">				
				
				<h3 class="sub-titulo">
					<?php echo $term->name; ?>
				</h3>
				
				<div class="lista">
							
					<div class="row">
						<?php 
							// LOOP DA FOTO DESTACADA
							if ( have_posts() ) : while( have_posts() ) : the_post();

							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			                $foto = $foto[0];
							
							//RECUPERANDO INSGREDIENTES
							$ingredientes = rwmb_meta('Casadopirogue_ingredientes');
							$unidade = rwmb_meta('Casadopirogue_unidade');
							$porcao = rwmb_meta('Casadopirogue_porcao');
							$conservantes = rwmb_meta('Casadopirogue_glutem-conservantes');
							$energetico = rwmb_meta('Casadopirogue_valor-energetico');
							$carboidratos = rwmb_meta('Casadopirogue_carboidratos');
							$proteinas = rwmb_meta('Casadopirogue_proteinas');
							$gorduras_totais = rwmb_meta('Casadopirogue_gorduras-totais');
							$gorduras_saturadas = rwmb_meta('Casadopirogue_gorduras-saturadas');
							$gorduras_transs = rwmb_meta('Casadopirogue_gorduras-trans');
							$fibra_alimentar = rwmb_meta('Casadopirogue_fibra-alimentar');
							$sodio = rwmb_meta('Casadopirogue_sodio');
							$calcio = rwmb_meta('Casadopirogue_calcio');
							$colesterol = rwmb_meta('Casadopirogue_colesterol');
							$ferro = rwmb_meta('Casadopirogue_ferro');	
							$precoDoProduto = rwmb_meta('Casadopirogue_preco'); 		
						
						 ?>
						<div class="col-md-6">
							
							
							<div class="lista-produtos">
							
							
								<a href="" class="link-produto"  
								data-foto="<?php echo $foto ?>"
								data-nome="<?php echo get_the_title() ?>" 
								data-ingredientes="<?php echo $ingredientes ?>" 
								data-unidade="<?php echo $unidade ?>" 
								data-modo-preparo="<?php echo get_the_content(); ?>" 
								data-toggle="modal" data-target=".bs-example-modal-lg" 
								title="CLIQUE PARA VER <?php echo get_the_title(); ?>"
								data-porcao="<?php echo $porcao ?>"  
								data-conservantes="<?php echo $conservantes ?>"   
								data-energetico="<?php echo $energetico ?>"  
								data-carboidratos="<?php echo $carboidratos ?>"  
								data-proteinas="<?php echo $proteinas ?>"  
								data-gorduras-totais="<?php echo $gorduras_totais ?>"  
								data-gorduras-saturadas="<?php echo $gorduras_saturadas ?>"  
								data-gorduras-transs="<?php echo $gorduras_transs ?>"  
								data-fibra-alimentar="<?php echo $fibra_alimentar ?>"  
								data-sodio="<?php echo $sodio ?>"  
								data-calcio="<?php echo $calcio ?>"  
								data-colesterol="<?php echo $colesterol ?>"  
								data-ferro="<?php echo $ferro ?>"
								data-precoDoProduto="<?php echo $precoDoProduto ?>">

								
								

								<?php echo get_the_title(); ?>	
															
								<span style="background: url(<?php echo $foto; ?>);"></span>
								</a>
													

							</div>
						</div>	
						<?php    endwhile; endif;  ?>					

					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- TABELA DO PRODUTO -->

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

  <div class="modal-dialog modal-lg">

    <div class="modal-content">

     	<div class="infoproduto">

     		<div class="row correcaoY">

				

				<!-- INFORAÇÕES DO PRODUTO -->

				<div class="col-md-6 correcaoX">

					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<h2 id="nome"></h2>

					

					<div class="ingredientes">

						<span>-Ingredientes:</span>

						<p id="ingredientes"></p>

						<p><i id="conservantes1"></i></p>

						<p><i id="conservantes2"></i></p>

					</div>



					<div class="modo-conservacao">

						<span>-Modo de preparo:</span>

						<p id="descricao"></p>

					</div>

					

					<div class="info">	

						<span>-Informações do pacote</span>							

						<p><?php echo $info_pacote ?></p>

						<p id="unidade"></p>

						<a href="#" class="link-modal" data-toggle="modal" data-target="#myModal2">Veja aqui as informações nutricional</a>

					</div>



					<div class="info">	

						<span>-Modo de Conservação:</span>

						<p><?php echo $conservacao ?></p>

						<p>Validade: <?php echo $validade ?></p>

					

					</div>

					<a href="https://delivery.casadopirogue.com.br/order" class="login">Faça o seu pedido <i class="fa fa-sign-in" aria-hidden="true"></i></a>

				</div>

				<div class="col-md-6">

					<div class="foto" id="foto-modal">
						<span id="precoDoProduto"></span>
					</div>

				</div>

				

			

			</div>

     	</div>

    </div>

  </div>

</div>



<!-- TABELA NUTROCIONAL -->

<div class="infoproduto">	

<div id="myModal2" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        

     

      <div class="modal-body">

        <div class="tabela">

			<p class="titulo-modal">Informações Nutricionais</p>

			<span id="Qtdporcao"></span>



			<table class="table">

				<tr>

					<th>Quantidade por porção </th>

					<th></th>

					<th>%VD*</th>

				</tr>	

				<tr class="bg">

					<td>Valor Energético</td>

					<td id="energetico1"></td>

					<td id="energetico2"></td>

				</tr>

				<tr>

					<td>Carboidratos</td>

					<td id="carboidratos1"></td>

					<td id="carboidratos2"></td>

				</tr>

				<tr class="bg">

					<td>Proteínas</td>

					<td id="proteinas1"></td>

					<td id="proteinas2"></td>

				</tr>

				<tr>

					<td>Gordura Totais</td>

					<td id="totais1"></td>

					<td id="totais2"></td>

				</tr>

				<tr class="bg">

					<td>Gordura Saturadas</td>

					<td id="saturadas1"></td>

					<td id="saturadas2"></td>

				</tr>

				<tr >

					<td>Gordura Trans</td>

					<td id="trans1"></td>

					<td id="trans2"></td>

				</tr>

				<tr class="bg" >

					<td>Fibra Alimentar</td>

					<td id="alimentar1"></td>

					<td id="alimentar2"></td>

				</tr>

				<tr >

					<td>Sódio</td>

					<td id="sodio1"></td>

					<td id="sodio2"></td>

				</tr>

				<tr class="bg" >

					<td>Cálcio</td>

					<td id="calcio1"></td>

					<td id="calcio2"></td>

				</tr>

				<tr>

					<td>Colesterol</td>

					<td id="colesterol1"></td>

					<td id="colesterol2"></td>

				</tr>

				<tr>

					<td class="bg">Ferro</td>

					<td id="ferro1"></td>

					<td id="ferro2"></td>

				</tr>

			</table>

			<p><?php echo $tabela_nutricional ?></p>

		</div>

      </div>

      <div class="modal-footer">

        <button type="button" class="close-footer" data-dismiss="modal">Close</button>

      </div>

    </div>



  </div>

</div>

</div>

<?php get_footer(); ?>
