$(function() {
    $(document).ready(function() {
        $("#carrossel-inicial").owlCarousel({
            items: 1,
            dots: true,
            loop: true,
            lazyLoad: true,
            mouseDrag: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            animateOut: 'fadeOut',
            smartSpeed: 450,
            autoplaySpeed: 4000,
        });
        var carrossel_destaque = $("#carrossel-inicial").data('owlCarousel');
        $('.navegacaoDestqueFrent').click(function() {
            carrossel_destaque.prev();
        });
        $('.navegacaoDestqueTras').click(function() {
            carrossel_destaque.next();
        });
    });
    $(document).ready(function() {
        $("#carrossel-quem-somos").owlCarousel({
            items: 4,
            dots: true,
            loop: true,
            lazyLoad: true,
            mouseDrag: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            animateOut: 'fadeOut',
            smartSpeed: 450,
            autoplaySpeed: 4000,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        });
        var carrossel_destaque = $("#carrossel-quem-somos").data('owlCarousel');
        $('.navegacaoDestqueFrent').click(function() {
            carrossel_destaque.prev();
        });
        $('.navegacaoDestqueTras').click(function() {
            carrossel_destaque.next();
        });
    });
    $('a#contato').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    $('a#produtos').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    $('a#contato-footer').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    $('.link-produto').click(function(e) {
        var porcao = $(this).attr('data-porcao');
        var energetico = $(this).attr('data-energetico').split(";");
        var unidade = $(this).attr('data-unidade').split(";");
        var carboidratos = $(this).attr('data-carboidratos').split(";");
        var proteinas = $(this).attr('data-proteinas').split(";");
        var totais = $(this).attr('data-gorduras-totais').split(";");
        var saturadas = $(this).attr('data-gorduras-saturadas').split(";");
        var transs = $(this).attr('data-gorduras-transs').split(";");
        var alimentar = $(this).attr('data-fibra-alimentar').split(";");
        var sodio = $(this).attr('data-sodio').split(";");
        var calcio = $(this).attr('data-calcio').split(";");
        var colesterol = $(this).attr('data-colesterol').split(";");
        var ferro = $(this).attr('data-ferro').split(";");
        $('#Qtdporcao').text(porcao);
        $('#energetico1').text(energetico[0]);
        $('#energetico2').text(energetico[1]);
        $('#carboidratos1').text(carboidratos[0]);
        $('#carboidratos2').text(carboidratos[1]);
        $('#proteinas1').text(proteinas[0]);
        $('#proteinas2').text(proteinas[1]);
        $('#totais1').text(totais[0]);
        $('#totais2').text(totais[1]);
        $('#saturadas1').text(saturadas[0]);
        $('#saturadas2').text(saturadas[1]);
        $('#trans1').text(transs[0]);
        $('#trans2').text(transs[1]);
        $('#alimentar1').text(alimentar[0]);
        $('#alimentar2').text(alimentar[1]);
        $('#sodio1').text(sodio[0]);
        $('#sodio2').text(sodio[1]);
        $('#calcio1').text(calcio[0]);
        $('#calcio2').text(calcio[1]);
        $('#colesterol1').text(colesterol[0]);
        $('#colesterol2').text(colesterol[1]);
        $('#ferro1').text(ferro[0]);
        $('#ferro2').text(ferro[1]);
        var modoPreparo = $(this).attr('data-modo-preparo');
        var ingredientes = $(this).attr('data-ingredientes');
        var conservantes = $(this).attr('data-conservantes').split(";");
        var foto = $(this).attr('data-foto');
        var nome = $(this).attr('data-nome');
        var precoDoProduto = $(this).attr('data-precoDoProduto');
        $('#foto-modal').css({
            "background": "url(" + foto + ")"
        });
        $('#nome').text(nome);
        $('#precoDoProduto').text(precoDoProduto);
        $('#descricao').text(modoPreparo);
        $('#ingredientes').text(ingredientes);
        $('#conservantes1').text(conservantes[0]);
        $('#conservantes2').text(conservantes[1]);
        $('#unidade').text(unidade);
    });
    $("a#fancy").fancybox({
        'titleShow': false,
        openEffect: 'elastic',
        closeEffect: 'elastic',
        closeBtn: true,
        arrows: true,
        nextClick: true
    });
    $(window).bind('scroll', function() {
        var alturaX = $(window).scrollTop()
        if (alturaX > 1) {
            $('#topo').css({
                'position': 'fixed'
            });
            $('#topo').css({
                'z-index': '2'
            });
        } else {
            $('#topo').css({
                'position': 'initial'
            })
        }
    });
    $("#Iconeligar").mouseenter(function() {
        $("#delivery").css({
            "background": "red",
            "color": "#FFF"
        })
    });
    $("#Iconeligar").mouseleave(function() {
        $("#delivery").css({
            "background": "#fff",
            "color": "#FD8A34"
        })
    });
});