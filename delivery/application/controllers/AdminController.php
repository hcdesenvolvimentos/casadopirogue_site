<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {
	private $page_title;

	public function __construct() {
		parent::__construct();
		$this->setTitle('Área do Administrador');
		$this->load->model('user_model', 'user');
		$this->load->model('order_model', 'order');
	}

	public function index() {

		$this->beforeAction();

		$this->setTitle('Dashboard ');

		$datas = array(
			'page_title' => $this->getTitle()
		);

		return $this->template->load('admin',  'admin/dashboard', $datas);
	}

	public function getOrder () {

		$this->beforeAction();
		$order_id = $this->uri->segment(3);
		$order = $this->order->get_all_orders($order_id);

		if (count($order) === 0) {
			flash($this, 'flashWarning', 'Pedido não encontrado!');
			redirect('admin/orders');
		}

		$this->setTitle('Detalhes do Pedido: <b>' . $order_id . '<b>');
		$datas = array('page_title' => $this->getTitle());
		$datas['orders']  = $this->order->get_all_orders($order_id);

		/*echo "<pre>";
		print_r($this->order->get_all_orders($order_id));
		echo "</pre>";*/
		return $this->template->load('admin',  'admin/order', $datas);
	}

	public function orders () {

		$this->beforeAction();

		$this->setTitle('Gerenciar Pedidos');

		$datas = array(
			'page_title' => $this->getTitle()
		);

		return $this->template->load('admin',  'admin/orders', $datas);
	}

	public function getOrders_json () {

		$user_id = (int) $this->input->get("user_id");

		$response = array();

		if (!$this->user->findAdminById($user_id)) {
			$response['status'] = 403;
			$response['message'] = 'Requisicao nao permitida!';

			echo  json_encode($response);
			return json_encode($response);
		}

		$response['status']      = 200;
		$response['message'] = 'Pedidos Carregados Com Sucesso!';
		$response['orders']      =$this->order->get_all_orders();

		echo  json_encode($response);
		return json_encode($response);
	}

	public function clients () {

		$this->beforeAction();

		$this->setTitle('Clientes Cadastrados');

		$datas = array(
			'page_title' => $this->getTitle()
		);

		$clients = $this->user->getAllClients ();

		$datas['clients'] = $clients;

		flash($this, 'flashSuccess', count($clients) . ' clientes cadastrados');
		return $this->template->load('admin',  'admin/clients', $datas);
	}

	private function getTitle() {
		return $this->page_title;
	}

	private function setTitle($title) {
		$this->page_title = $title;
	}

	private function beforeAction() {

		if (!$this->session->has_userdata('current_user')) { return redirect('login'); }

		if (!$this->user->findAdminByEmail($this->get_current_user ()['email'])) {
			flash($this, 'flashWarning', 'Você não possui privilégios para acessar esta página!');
			return redirect('login');
		}
	}

	private function get_current_user () {
		return $this->session->get_userdata('current_user')['current_user'];
	}
}