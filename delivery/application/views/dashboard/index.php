 <p class="mensagem-conta">Olá <span><?php echo   $this->session->get_userdata('current_user')['current_user']['name']; ?></span> seja bem vindo a sua conta na <i>Casa do Pirogue!</i> Para realiza alterações em sua conta utilize o menu ao lado.</p>

<script>
	
	$('.modalTutorial').css({'height':$(document).height()});
		$(window).resize(function(){
			$('.modalTutorial').css({'height':$(document).height()});
		});
		
		setTimeout(function(){
		  $(".modalTutorial").fadeIn("slow");
		}, 2000);
		$(".sairModal,.closes,#sair").click(function(e) {
			 $(".modalTutorial").slideUp("slow");
		});
		$(".buttonTutorial").click(function(e) {
		   $(".areaCarrossel").slideDown("slow");
		   $(".modalRecepecicao").slideUp("slow");
		   	
				$("#carrossel-tutorial").owlCarousel({
					items : 1,
			        dots: true,
			        loop: false,
			        lazyLoad: true,
			        mouseDrag: true,
			        autoplay:false,
				    autoplayTimeout:5000,
				    autoplayHoverPause:true,
				    // animateOut: 'fadeOut',
				    smartSpeed: 450,
				    autoplaySpeed: 4000,
				});
				var carrossel_tutorial = $("#carrossel-tutorial").data('owlCarousel');
				$('.navegacaotutorialFrent').click(function(){ carrossel_tutorial.prev(); });
				$('.navegacaotutorialTras').click(function(){ carrossel_tutorial.next(); });
		});

</script>