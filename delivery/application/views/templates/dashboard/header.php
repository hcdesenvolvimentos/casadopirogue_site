<?php $current_user = $this->session->get_userdata('current_user')['current_user']['id'] ?>
<?php $current_user_picture = (isset($this->session->get_userdata('current_user')['current_user']['picture'])) ? $this->session->get_userdata('current_user')['current_user']['picture'] : null ?>
<?php $isAdmin = (isset($this->session->get_userdata('current_user')['current_user']['admin'])) ?  $this->session->get_userdata('current_user')['current_user']['admin'] : false ?>
<!DOCTYPE html>
<html lang="pt">
<head>
      <meta http-equiv="Content-type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Casa do Pirogue<?php if (isset($page_title)) { echo ' | '.$page_title; } ?></title>

      <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Roboto+Condensed:400,700italic,400italic,700,300,300italic' rel='stylesheet' type='text/css'>

      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css") ?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/font-awesome.min.css") ?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/owl.carousel.min.css") ?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/animate.css") ?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/jquery.steps.css") ?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/jquery.toast.css") ?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/site.css") ?>" />
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url("assets/img/casadopirogue.icon.ico") ?>" />

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="assets/js/bootstrap.min.js"></script>
      <script src="assets/js/geral.js"></script>
</head>
<body>

   <div class="modalTutorial">

      <div class="modalRecepecicao">
         <strong>Olá <i><?php echo   $this->session->get_userdata('current_user')['current_user']['name']; ?></i>, é sua primeira vez aqui?</strong>
         <p>Podemos te ajudar!</p>

         <button class="buttonTutorial">Clique aqui e confira como efetuar sua compra!</button>
         <b class="closes"><i class="fa fa-times" aria-hidden="true"></i></b>
      </div>
      <div class="areaCarrossel" style="display: none;">
         <button class="sairModal">Sair do tutorial <i class="fa fa-times" aria-hidden="true"></i></button>
         <button class="navegacaotutorialFrent buttons"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
         <button class="navegacaotutorialTras buttons"><i class="fa fa-angle-right" aria-hidden="true"></i></button>

         <div id="carrossel-tutorial" class="owl-Carousel">
            <div class="item">
               <span>Olá <strong><?php echo   $this->session->get_userdata('current_user')['current_user']['name']; ?></strong>, seja bem vindo!</span>
               <p><strong>1.</strong> Para sair basta clicar em "Sair do tutorial" no topo da sua tela <i class="fa fa-arrow-up" aria-hidden="true"></i>.</p>
               <p><strong>2.</strong> Para navegar basta clicar nas flexas que ficam posicionada no canto da sua tela. <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
               <p><strong>3.</strong> Podemos começar? <i class="fa fa-play-circle-o" aria-hidden="true"></i></p>

            </div>
            <div class="item">
               <p> <strong>Meus dados cadastrais <i class="fa fa-user" aria-hidden="true"></i></strong> </p>
               <img src="assets/img/dadospessoais.jpg" alt="">
            </div>
            <div class="item">
               <p> <strong>Meus Endereços <i class="fa fa-map-marker" aria-hidden="true"></i></strong> </p>
               <img src="assets/img/endereco.jpg" alt="">
            </div>
            <div class="item">
               <p> <strong> Escolha dos Produtos <i class="fa fa-shopping-basket" title="Escolha seu Produtos"></i> </strong> </p>
               <img src="assets/img/compra1.jpg" alt="">
            </div>
            <div class="item">
               <p> <strong>  Modo de Entrega <i class="fa fa-truck" title="Modo de Entrega"></i></strong> </p>
               <img src="assets/img/compra2.jpg" alt="">
            </div>
            <div class="item">
               <p> <strong>  Confirmação do Pedido <i class="fa fa-thumbs-o-up" title="Modo de Entrega"></i></strong> </p>
               <img src="assets/img/compra3.jpg" alt="">
            </div>
            <div class="item">
               <p> <strong> Confirmação do Pedido <i class="fa fa-thumbs-o-up" title="Modo de Entrega"></i></strong> </p>
               <img src="assets/img/compra4.jpg" alt="">
            </div>
            <div class="item">
               <span>Fim!</span>
               <p>Obrigado <b><?php echo   $this->session->get_userdata('current_user')['current_user']['name']; ?></b> por realizar seu pedido na Casa Do Pirogue <i class="fa fa-home" aria-hidden="true"></i></p>
               <p>O período para retirada/entrega do pedido é de até dois dias após nossa confirmação. <i class="fa fa-info-circle" aria-hidden="true"></i></p>
               <p>Entraremos em contato para à confirmação do seu pedido <i class="fa fa-check-square" aria-hidden="true"></i></p>
               <button id="sair"> Clique aqui para Sair <i class="fa fa-times" aria-hidden="true"></i></button>
            </div>
         </div>

      </div>
   </div>

<!-- TOPO -->
   <div class="topo" id="topo">
      <nav class="navbar" role="navigation">

         <div class="container">

            <div class="row">
               <div class="col-md-3">
                  <!-- LOGO -->
                  <a href="http://casadopirogue.com.br/" title="Casa do Pirogue">
                     <h1>
                        Casa do Pirogue
                     </h1>
                  </a>
                  <!-- LOGOTIPO / MENU MOBILE-->
                  <div class="navbar-header">


                     <!-- MENU MOBILE TRIGGER -->
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>

                  </div>
               </div>
               <div class="col-md-9 text-center">
                  <!-- MENU -->
                  <nav class="collapse navbar-collapse" id="collapse">

                     <ul class="nav navbar-nav">

                       <li><a href="http://casadopirogue.com.br/">Home</a></li>
                       <li><a href="http://casadopirogue.com.br/">Nossos Produtos</a></li>
                       <li><a href="<?php base_url() ?>">Pedidos Online</a></li>
                       <li><a href="http://casadopirogue.com.br/sobre-nos/">Sobre nós</a></li>
                       <li><a href="http://casadopirogue.com.br/#area-contato">Contato</a></li>

                     </ul>

                  </nav>
               </div>
            </div>
         </div>
      </nav>
   </div>
<div class="container">
      <div class="pg pg-minha-conta">
           <div class="container">
               <center><p class="titulo">Área do Cliente <i class="fa fa-user" aria-hidden="true"></i></p></center>
               <div class="row borda">
                  <div class="col-md-3">
                     <div class="sidebar" id="sidebar-altura">
                        <div class="perfil">
                           <div class="foto" title="<?php echo $this->session->get_userdata('current_user')['current_user']['name']; ?>" style="background-image: url(<?php echo ($current_user_picture === null) ? base_url('assets/img/user.png') : $current_user_picture ?>);" >
                           </div>
                           <span title="user" ><?php echo   $this->session->get_userdata('current_user')['current_user']['name']; ?></span>
                           <p><?= anchor('logout', 'Sair', 'title="Sair" class="sair"') ?> <span><i class="fa fa-sign-out" aria-hidden="true"></i> </span></p>
                        </div>
                        <?php if ($isAdmin)  { ?>
                           <a title="Ir para a Área Adminstrativa" href="<?= base_url('admin') ?>" class="menu">Área Adminstrativa</a>
                        <?php } ?>
                        <a href="<?= base_url('order') ?>" class="menu">Fazer Pedido</a>
                        <a href="<?= base_url('my-orders') ?>" class="menu">Meus pedidos</a>
                        <a href="<?= base_url('address') ?>" class="menu">Meus Endereços</a>
                        <a href='<?= base_url("account/{$current_user}/edit") ?>' class="menu">Meus dados cadastrais</a>
                     </div>
                  </div>
                  <div class="col-md-9" id="altura">
                        <div class="panel-body">
                        <?php require_once '_flashes.php'; ?>
                        <div id="ajax-flashes"></div>
