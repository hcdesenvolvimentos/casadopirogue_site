<?php $current_user = $this->session->get_userdata('current_user')['current_user']?>
<!DOCTYPE html>
<html lang="pt">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Casa do Pirogue<?php if (isset($page_title)) { echo ' | '.$page_title; } ?></title>

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Roboto+Condensed:400,700italic,400italic,700,300,300italic' rel='stylesheet' type='text/css'>

	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css") ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/font-awesome.min.css") ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/owl.carousel.min.css") ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/animate.css") ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/jquery.steps.css") ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/jquery.toast.css") ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/site.css") ?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/admin.css") ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url("assets/img/casadopirogue.icon.ico") ?>" />
</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" title="Casa do Pirogue | Admin">Casa do Pirogue | <?php echo $current_user['name'] ?> </a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo base_url('dashboard') ?>">Área do Cliente</a></li>
					<li><a href="<?php echo base_url('admin') ?>"><i class="fa fa-user" aria-hidden="true"></i> Área do Admin</a></li>
					<li><a href="<?php echo base_url('admin/orders') ?>"><i class="fa fa-cogs" aria-hidden="true"></i> Gerenciar Pedidos</a></li>
					<li><a href="<?php echo base_url('admin/clients') ?>"><i class="fa fa-users" aria-hidden="true"></i> Clientes</a></li>
					<li><a href="<?php echo base_url('logout') ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">
	<br>
	<?php require_once '_flashes.php'; ?>
	<div id="ajax-flashes"></div>
	<div class="page-header">
		<h2> <?php echo (isset($page_title)) ? $page_title : '' ?> </h2>
		<div class="pull-right" id="current_user" data-current-user-id="<?php echo $current_user['id']  ?>">
			Seja bem-vindo à área do administrador, <b><?php echo $current_user['name']  ?></b>!
		</div>
	</div>