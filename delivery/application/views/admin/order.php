<?php foreach ($orders as $key => $order) { ?>
	<?php $datestring = 'Year: %Y Month: %m Day: %d - %h:%i %a'; ?>
	<div class="row">
		<div class="col-xm-12 col-sm-12">
			<div class="col-xm-3 col-sm-3 col-height-360">
				<div class="panel panel-default col-inherit-height" >
					<div class="panel-heading"><b><h4> Informações do Cliente </h4></b></div>
					<div class="panel-body">
						<p>Nome: <?php  echo $order->user->name ?> </p>
						<p>E-mail: <?php  echo $order->user->email ?> </p>
						<p>Telefone 1: <?php  echo $order->user->phone1 ?> </p>
						<p>Telefone 2: <?php  echo $order->user->phone2 ?> </p>
						<br><br><br>
					</div>
				</div>
			</div>
			<?php  if (isset($order->address)) { ?>
				<div class="col-xm-4 col-sm-4 col-height-360">
					<div class="panel panel-default col-inherit-height">
						<div class="panel-heading"><h4> Informações Endereço de Entrega </h4> </div>
						<div class="panel-body">
							<p>Nome do Endereço: <?=  $order->address->address_name ?> </p>
							<p><span id="rua"><?=  $order->address->street.', '.$order->address->number.'</span> - '.$order->address->complement ?></p>
							<p>Bairro: <span id="bairro"><?=  $order->address->neighborhood ?></span></p>
							<p>Cidade: <span id="cidade"><?=  $order->address->city ?></span></p>
							<p>CEP: <span id="cep"><?=  $order->address->zip_code ?></span></p>
							<p>Estado: <span id="estado"><?=  $order->address->uf ?></span></p>
						</div>
					</div>
				</div>

				<div class="col-xm-5 col-sm-5 col-height-360">
					<div class="panel panel-default col-inherit-height">
						<div class="panel-heading"><h4>Mapa Rota de Entrega </h4> </div>
						<div class="panel-body">
							<div id="panel-content">
								<div id="panel-content-map" style="height: 200px">
									<div id="route-map"  style="width:100%; height: 100%"></div>
								</div>
							</div>

						</div>
						<div class="panel-footer">
								<a href="#" id="show-modal-map-route" class="btn btn-default btn-lg btn-block" title="Visualizar mapa da rota de entraga" data-toggle="modal" data-target="#modal-route-map-delivery">
									<i class="fa fa-map-marker" aria-hidden="true"></i>
									Exibir Mapa
								</a>
							</div>
					</div>
				</div>
			<?php }  ?>
		</div>
	</div>
		<hr>
		<div class="panel-group" role="tablist">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="collapseListGroupHeading<?= $order->id ?>">
					<h4 class="panel-title">
						<div class="table-responsive">
							<table class="table table-responsive">
								<thead>
									<tr>
										<th>Nº Pedido</th>
										<th>Data</th>
										<th>Valor Total</th>
										<th>Modo de Entrega</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td ><?= $order->id ?></td>
										<td ><?= date("d/m/Y", strtotime($order->delivery_date)) ?></td>
										<td >R$ <?= number_format($order->total,2,",","." ); ?></td>
										<td><?= $order->delivery ?></td>
										<td> <span class="label label-<?= status_label($order->status) ?>"><?=  status_message($order->status) ?></span></td>
									</tr>
								</tbody>
							</table>
						</div>
						<a class="" role="button" data-toggle="collapse" href="#collapseListGroup<?= $order->id ?>"  aria-controls="collapseListGroup<?= $order->id ?>">Detalhes...</a>
					</h4>
				</div>

				<div id="collapseListGroup<?= $order->id ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapseListGroupHeading<?= $order->id ?>" >
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-8 col-md-8">
								<div class="table-responsive">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Nº</th>
												<th>Categoria</th>
												<th>Item</th>
												<th>Quantidade</th>
												<th>Valor Unitário</th>
												<th>Subtotal</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($order->itens as $index => $item) { ?>
												<tr>
													<td><?= $item->item_id ?></td>
													<td><?= $item->category ?></td>
													<td><?= $item->name ?> </td>
													<td><?= $item->amount ?> </td>
													<td>R$ <?=  number_format($item->package_price,2,",","." ); ?> </td>
													<td>R$ <?= number_format(($item->amount * $item->package_price),2,",","." );  ?> </td>
												</tr>
												<?php } ?>
												<tr>
													<td colspan="4"><b>Frete (Distância: <?= $order->distance ?>km)</b></td>
													<td>R$ <?= number_format($order->freight,2,",","." ); ?></td>
												</tr>
												<tr class="success">
													<td colspan="4"><b>Total</b></td>
													<td>R$ <?= number_format($order->total,2,",","." ); ?></td>
												</tr>
												<tr>
													<td colspan="4"><b>Modo de Pagamento</b></td>
													<td><?= $order->payment_mode ?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-xs-1 col-md-1"></div>
								<?php if ($order->status === 'Pendente')  { ?>
									<div class="col-xs-3 col-md-3">
										<div class="well center-block">
											<a role="button" href="/delivery/order/to_approve/<?php echo $order->approve_order_link ?>" class="btn btn-success  btn-block" title="Aceitar Pedido">
											<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
											Aprovar Pedido
											</a>
											<br><br>
											<a role="button" href="/delivery/order/to_disapprove/<?php echo $order->disapprove_order_link ?>" class="btn btn-danger  btn-block" title="Rejeitar Pedido">
											<i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
											Rejeitar Pedido
											</a>
										 </div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>



<div class="modal fade" id="modal-route-map-delivery" tabindex="-1" role="dialog" aria-labelledby="Rota de Entrega">
	<div class="modal-dialog modal-lg">
		<div id="modal-content-map" class="modal-content" style="height:500px;">
			<div class="modal-header text-center"><i class="fa fa-map-marker" aria-hidden="true"></i> Rota de Entrega</div>

		</div>
	</div>
</div>
