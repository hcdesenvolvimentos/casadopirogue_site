<section>
<div class="table-responsive">
   <table class="table table-striped" id="manage-orders">
      <thead>
         <tr>
            <th  colspan="3" class="text-center" >Ações</th>
            <th>Nº</th>
            <th>Cliente</th>
            <th>Endereço de Entrega</th>
            <th>Modo</th>
            <th>Quantidade Itens</th>
            <th>Total</th>
            <th colspan="2" class="text-center"> Status </th>
         </tr>
      </thead>
      <tbody>

      </tbody>
   </table>
</div>

<div id="div-users-modals"></div>
<div id="div-address-modals"></div>
</section>
