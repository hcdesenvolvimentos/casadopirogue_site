<section>
<div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Data de registro</th>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Telefone 1</th>
				<th>Telefone 2</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($clients as $client) { ?>
				<tr>
					<td><?php echo  date("d/m/Y", strtotime($client->created_at)) ?></td>
					<td><?php echo  $client->name ?></td>
					<td><?php echo  $client->email ?></td>
					<td><?php echo  $client->phone1 ?></td>
					<td><?php echo  $client->phone2 ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
</section>
