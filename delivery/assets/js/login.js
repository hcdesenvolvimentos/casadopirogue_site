$(document).ready(function() {

	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/pt_BR/sdk.js', function(){
		FB.init({
			appId      : '793550200780191',
			cookie     : true,  // enable cookies to allow the server to access
			status     : true,  // the session
			xfbml      : true,  // parse social plugins on this page
			version    : 'v2.6' // use version 2.6
	});


		function auth (response) {

			FB.login(function(response) {
				if (response.authResponse) {
					console.log('here!');
					FB.api('/me?fields=id,name,email, first_name, last_name, birthday, location', function(user) {
						console.log(user);
						user.token = response.authResponse.accessToken;
						var url = "/" + user.id + "/picture?type=large";

						FB.api(url, function (response) {
							console.log('Get Picture');
							user.picture = (response && !response.error) ? response.data.url : null;
							facebook_sign_in(user);
						});
					});
				} else {
					console.log('User cancelled login or did not fully authorize.');
				}
			}, {scope: 'email'});
		}


		function facebook_api_login () {
			FB.getLoginStatus(auth);
		}


		function facebook_sign_in (user) {

			console.log('Sign in');

			var url = '/delivery/login/facebook';

			$.ajax({
				type: 'POST',
				url: url,
				data: {'user_id' : user.id, 'user_email': user.email, 'user_name' : user.name, 'user_token' : user.token, 'user_picture' : user.picture},
				success: function(data, status) {
					console.log(data);
					window.location.href = "/delivery/dashboard";
				},
				error: function(data, status) {
					console.log(data);
					flash('error', 'Ocorreu um erro ao tentar a autenticação no sistema via facebook!');
					return false;
				},
			});
		}

		function facebook_sign_out () {

			var url = 'logout/facebook';

			$.ajax({
				type: 'GET',
				url: url,
				success: function(data, status) {
					var description_mode = (mode === 'Entrega') ? 'Solicitar entrega do pedido em meu endereço principal.' : 'Retirar o pedido no estabelecimento.';
					flash('success', 'Modo atualizado com sucesso para:  '+description_mode);
				},
				error: function(data, status) {
					alert('Ocorreu um erro ao tentar deslogar no sistema! ' + data);
				},
			});
		}

		$("#facebook-sign-in").on('click', function(event) {
			FB.getLoginStatus(facebook_api_login);
		});

	});
});