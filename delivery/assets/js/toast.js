$(document).ready(function (){

	window.toast = function (icon, header, content) {

		$.toast({
			icon: icon,
			heading: header,
			text: content,
			position: 'top-right',
			showHideTransition: 'plain',
			stack: 10,
		});

		return true;
	}

});