$(document).ready(function (){

	if ($("#manage-orders").size()  > 0) {
		window.timer = setInterval(function () {
			updateTableOrders();
			 if ($("#manage-orders").size() <= 0) {
	      			clearInterval(window.timer);
	       		}

		}, 5000);
	}

	updateTableOrders();

	function updateTableOrders() {
		if ($("#manage-orders").size() > 0) {
			var user_id = $('#current_user').data('current-user-id');
			console.log('Nova Requisição Tabela de Pedidos');
			var url = 'orders/json';
			//toast('info', 'Novo Pedido', 'Um novo pedido foi recebido!');
			$.ajax({
				type: 'GET',
				dataType: 'json',
				data: {'user_id': user_id},
				contentType: "application/json; charset=utf-8",
				url: url,
				success: function(data, status) {
					flash('success', 'Total de pedidos: ' + data.orders.length);
					//console.log(data.message);
					if (data.status === 200) {

						var orders = $(data.orders);
						var orders_table = $('#manage-orders tbody');
						var order_count = orders_table.children('tr').size();

						console.log(order_count);
						console.log(orders.length);
						if ((order_count > 0) && (orders.length > order_count)) {
							toast('info', 'Novo pedido recebido', 'Você recebeu uma nova requisição de pedido!');
						}

						//clean table
						orders_table.empty();
						$("#div-users-modals").empty();
						$("#div-address-modals").empty();

						orders.each(function(index, order) {
							//console.log(order);
							createUserModal(order);

							var status = defineStatusColor(order);
							var address = '';
							if (order.address_id != null) {
							 	address = "<a href='#' title='Visualizar Informações do Endereço de entrega' data-toggle='modal' data-target='#modal-address-order-" + order.id +"'>" + order.address.address_name + "</a>";
							 	createAddressModal(order);
							}

							var order_details = "<a href='orders/"+order.id+"/details' title='Abrir Detalhes do Pedido'> Ver Detalhes </a>";


							var tr = "<tr class="+status.class+">" +
									"<td>" + status.action + "</td>" +
									"<td >" + order.id + " ("+order_details+")</td>" +
									"<td ><a href='#' title='Visualizar Informações do Cliente' data-toggle='modal' data-target='#modal-users-order-" + order.id +"'>" + order.user.name + "</a></td>" +
									"<td >" + address + "</td>" +
									"<td >" + order.delivery + "</td>" +
									"<td >" + order.itens_amount + "</td>" +
									"<td >" + order.total + "</td>" +
									"<td >" + status.label + "</td>" +
									"</tr>";
							orders_table.append(tr);
						});


					}
				},
				error: function(data, status) {
					/*if (data.status === 500) {
						flash('error', data.msg);
						console.log(data);
						return  false;
					}*/
				},
			});
		}
	}

	function defineStatusColor (order) {
		var status = {};
		status.class = 'success'
		status.label = '<td ><span class="label label-success"><i class="fa fa-check-square-o" aria-hidden="true" title="Pedido Aprovado"></i> Pedido Aceito</span></td>';
		status.action = '<td></td><td></td>';

		switch(order.status) {
		    case 'Aprovado':
		          return status;
		    case 'Cancelado':
		    	status.class = 'danger';
		    	status.label = '<td><span class="label label-danger"><i class="fa fa-times" aria-hidden="true" title="Pedido Rejeitado"></i> Pedido Rejeitado</span></td>';
		    	return status;
		    case 'Pendente':
		    	status.class = 'warning';
		    	status.label = '<td><span class="label label-warning"><i class="fa fa-question-circle-o" aria-hidden="true" title="Aguardando Aprovação"></i> Aguardando Aprovação</span></td>';
		    	var btn_accept   = '<td><a  href="/delivery/order/to_approve/'+order.approve_order_link+'" role="button" class="btn btn-success btn-xs" aria-label="Aceitar Pedido" title="Aceitar Pedido"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Aceitar Pedido</a></td>'
		    	var btn_reject     = '<td><a  href="/delivery/order/to_disapprove/'+order.disapprove_order_link+'" role="button" class="btn btn-danger btn-xs" aria-label="Rejeitar Pedido" title="Rejeitar Pedido"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Rejeitar Pedido</span></a></td>'
		    	status.action = btn_accept + btn_reject;
		    	return status;
		    default:
		        return status;
		}
	}

	function createUserModal(order) {

		var div_modals = $("#div-users-modals");
		var modal_html = '';

		modal_html = '<div class="modal fade" id="modal-users-order-' + order.id +'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
					  '<div class="modal-dialog modal-sm">' +
					        '<div class="modal-content">' +
					        order.user.name +
					        '</div>' +
					     '</div>' +
				    '</div>';

		div_modals.append(modal_html);
	}

	function createAddressModal(order) {

		var div_modals = $("#div-address-modals");
		var modal_html = '';

		modal_html = '<div class="modal fade" id="modal-address-order-' + order.id +'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
					  '<div class="modal-dialog modal-sm">' +
					        '<div class="modal-content">' +
					         order.address.address_name +
					        '</div>' +
					     '</div>' +
				    '</div>';

		div_modals.append(modal_html);
	}

	showRouteMap ();

	$("#show-modal-map-route").click(function(event) {
		var div_map = $("#route-map");
		var div_modal_map = $("#modal-content-map");
		div_map.appendTo(div_modal_map);
	});

	$('#modal-route-map-delivery').on('hide.bs.modal', function () {
	    	var div_map = $("#route-map");
		var div_panel_map = $("#panel-content-map");
		div_map.appendTo(div_panel_map);
	});


	function showRouteMap () {
		if ($("#modal-route-map-delivery").size() > 0) {

			var destino = $("#rua").text()+' - '+$("#bairro").text()+' '+$("#cidade").text()+'/'+$("#estado").text()+' - '+$("#cep").text();
			var origem = 'Rua Prof. Rodolfo Belz, 369 - Santa Cândida Curitiba - PR';
			var directionsService = new google.maps.DirectionsService();
			var request = {
				origin:origem,
				destination: destino,
				travelMode: google.maps.TravelMode.DRIVING
			};

			var directionsDisplay = new google.maps.DirectionsRenderer;
			var map = new google.maps.Map(document.getElementById('route-map'));

			directionsDisplay.setMap(map);

			google.maps.event.addDomListener(window, "resize", function() {
					console.log('resize!');
					var center = map.getCenter();
					google.maps.event.trigger(map, "resize");
					map.setCenter(center);
			});

			directionsService.route(request, function(response, status) {

				if (status == google.maps.DirectionsStatus.OK) {
					console.log(response);
					directionsDisplay.setDirections(response);
					return response;
				} else {
					console.log(response);
    	 				return false;
				}
			});

		}
	}

});