<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */
// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_casadopirogue_site');
/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');
/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');
/** nome do host do MySQL */
define('DB_HOST', 'localhost');
/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');
/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');
/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@?+,7MCmb0# p!h+(hm1A[fE>fZ:Z-D[k: k}3[!Ni[B*V&.=ID[aUmGbygK>.s^');
define('SECURE_AUTH_KEY',  ')*t_xEKUm;CO]f<>T83*LIvC!A<Z,E._<U@YWMq8H{MTH%0V>k;y5x@1mh8pT+C4');
define('LOGGED_IN_KEY',    '8`z0pChc2C!8=#-@#%F>w,JoAKLryE(0w%cGjYO(Sr6.W+o9tblh/z&/(VWlu0MQ');
define('NONCE_KEY',        '#%K_dfB]TwDJ~BKgJ9t`n(/xHDh<eqUHB<)V@Z=/j/hh^C}icCcG%mhvfJL|{M/N');
define('AUTH_SALT',        'P#_bBM#|LTwbQ*f]IzWe[57}V1^@:s/6!iBUX(<z`6[WMwH452gRDTbko?eK1-(/');
define('SECURE_AUTH_SALT', ',j0L_$R0QAOPl&/_x?}s=%9*h#J!Z=ntE^.tQ6#<Yz5jW?Nv7}@3,nx,V+71,yFY');
define('LOGGED_IN_SALT',   '8MuHG6 KS`X9dzi}Cc_/k1Iap[+(eMj/#I_F~-T%pR?c26:[ROQB#$G},K0_E=dk');
define('NONCE_SALT',       'oV2R*5.-qqW8wAA7}7=^C/Qk}f]J#F<}^clmU6x@:7i;b6ue3MIDY?D_hqQlue3z');
/**#@-*/
/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'cp_';
/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);
/* Isto é tudo, pode parar de editar! :) */
/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);